<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/spipicious?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_tags' => 'Pridať štítky',
	'auteur_tag_usage' => 'Tento autor použil tento štítok @nb@-krát',

	// C
	'cfg_description' => 'Tento zásuvný modul vám umožňuje jednoducho pridávať štítky k objektom.',

	// E
	'explication_ajout_tag' => 'Ak chcete pridať niekoľko štítkov naraz, použite oddeľovač: "@separateur@".',
	'explication_suppression_tag' => 'Vyberte štítky v zozname a pošlite formulár, aby boli odstránené.',

	// L
	'label_tags' => 'Štítky',
	'liste_auteurs_tag' => 'Tento štítok používa:',

	// M
	'message_aucun_tags' => 'Žiaden štítok nie je priradený',

	// N
	'no_tags_associes' => 'Nemáte priradené žiadne štítky',
	'nom_groupe_tag' => 'Priradená skupina kľúčových slov',

	// T
	'tag_ajoute' => 'Pridali ste štítok:<br />@name@',
	'tag_deja_present' => 'Tento štítok ste už niekedy pridali.',
	'tag_supprime' => 'Odstránili ste štítok:<br />@name@',
	'tags' => 'Štítky:',
	'tags_ajoutes' => 'Pridali ste @nb@ štítkov:<br />@name@',
	'tags_supprimes' => 'Odstránili ste @nb@ štítkov:<br />@name@',
	'title_tag_utilise_nb' => 'Tento štítok priradili @nb@ ľudia.',
	'title_tag_utilise_un' => 'Tento štítok priradil jeden človek.',
	'types_utilisateurs' => 'Kto má prístup k formuláru a môže pridávať štítky?',

	// V
	'vos_tags' => 'Vaše štítky (zmena)'
);
