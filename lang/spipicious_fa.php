<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/spipicious?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add_tags' => 'افزودن تگ‌ها',
	'auteur_tag_usage' => 'اين نويسنده@nb@  بار از اين تگ استفاده كرده',

	// C
	'cfg_description' => 'اين پلاگين به بازديدكنندگان اجازه مي‌دهد تا به سادگي به مقاله‌ها تگ اضافه كنند.',

	// E
	'explication_ajout_tag' => 'براي افزودن چندين تگ در يك زمان از اين جداساز استفاده كنيد: "@separateur@".',
	'explication_suppression_tag' => 'تگ‌هاي خود را از فهرست زير برگزينيد و براي حذف آن‌ها معتبر كنيد.',

	// L
	'label_tags' => 'تگ‌ها:', # MODIF
	'liste_auteurs_tag' => 'اين تگ مورد استفاده قرار گرفته به وسيله‌ي : ',

	// M
	'message_aucun_tags' => 'هيچ تگ وابسته‌اي نيست.',

	// N
	'no_tags_associes' => 'كليدواژه‌ي مربوطه را نداريد',
	'nom_groupe_tag' => 'گروه‌واژه‌ي مربوطه',

	// T
	'tag_ajoute' => 'تگي را افزوديد: @br />@name>   ',
	'tag_deja_present' => 'اين تگ را افزوديد.',
	'tag_supprime' => 'اين تگ را حذف كرديد: @br />@name> ',
	'tags' => 'تگ‌ها : ',
	'tags_ajoutes' => 'شما @nb@ تگ را اضافه كرديد: @br />@name>',
	'tags_supprimes' => 'شما @nb@ تگ را حذف كرديد: @br />@name> ',
	'title_tag_utilise_nb' => 'اين كليدواژه به وسيله‌ي @nb@ نفر افزوده شده',
	'title_tag_utilise_un' => 'اين كليدواژه به وسيله‌ي يك نفر وابسته شده',
	'types_utilisateurs' => 'چه كسي‌ به فرم افزودن تگ دسترسي دارد؟',

	// V
	'vos_tags' => 'نگ‌هاي شما (اصلاح)'
);
