<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-spipicious?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spipicious_description' => 'Permite a los visitantes autentificados añadir etiquetas (palabras clave) a los diferentes objetos.
_ Las palabras claves se añaden en un grupo configurable (por defecto ’{{- tags -}}’)
_ Icono de [Pawel Kadysz->http://oneseventyseven.com/]',
	'spipicious_slogan' => 'Etiquetar todos los objetos'
);
